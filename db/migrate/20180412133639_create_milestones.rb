class CreateMilestones < ActiveRecord::Migration[5.2]
  def change
    create_table :milestones do |t|
      t.string :name
      t.text :desc
      t.date :target_date

      t.timestamps
    end
  end
end
