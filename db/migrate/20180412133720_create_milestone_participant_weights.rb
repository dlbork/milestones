class CreateMilestoneParticipantWeights < ActiveRecord::Migration[5.2]
  def change
    create_table :milestone_participant_weights do |t|
      t.integer :milestone_id
      t.integer :participant_id
      t.float :weight

      t.timestamps
    end
  end
end
