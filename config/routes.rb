Rails.application.routes.draw do
  resources :participants
  resources :milestones
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
