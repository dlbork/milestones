json.extract! participant, :id, :name, :initials, :created_at, :updated_at
json.url participant_url(participant, format: :json)
