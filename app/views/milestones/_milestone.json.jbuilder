json.extract! milestone, :id, :name, :desc, :target_date, :created_at, :updated_at
json.url milestone_url(milestone, format: :json)
